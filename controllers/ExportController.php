<?php
/**
 * ExportController class
 */
namespace LimesurveyExport\Controller;

use org\jsonrpcphp\JsonRPCClient;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use LimesurveyExport\CsvParser;

/**
 * Controller used for CSV exports
 */
class ExportController
{

    /**
     * Return an error message
     *
     * @param  Response $response HTTP response
     * @param  string   $message  Error message
     * @param  int      $code     HTTP error code
     *
     * @return Response HTTP response
     */
    private function error(Response $response, $message, $code)
    {
        $response->getBody()->write($message);

        return $response->withStatus($code)->withHeader('Content-type', 'text/plain');
    }

    /**
     * Output survey responses into a CSV file
     * @param  Request  $request  HTTP request
     * @param  Response $response HTTP response
     * @param  array    $args     Additional arguments specified in URL
     * @return Response
     */
    public function exportCsv(Request $request, Response $response, array $args)
    {

        $params = $request->getParams();
        if (!isset($params['token']) || $params['token'] != TOKEN) {
            $response->getBody()->write("Invalid token");

            return $response->withStatus(403)->withHeader('Content-type', 'text/plain');
        }

        $jsonRpc = new JsonRPCClient(LIMESURVEY_URL.'/admin/remotecontrol');
        $csv = $jsonRpc->export_responses(
            $jsonRpc->get_session_key(LIMESURVEY_USER, LIMESURVEY_PASS),
            $args['id'],
            'csv',
            'fr',
            'complete',
            'full',
            'long'
        );

        if (is_string($csv)) {
            try {
                $parser = new CsvParser(base64_decode($csv));
                $response->getBody()->write($parser->getGoogleSheetCsv());

                return $response->withHeader('Content-type', 'text/csv');
            } catch (\InvalidArgumentException $e) {
                return $this->error($response, $e->getMessage(), 400);
            }
        } else {
            return $this->error($response, "Can't find this form", 404);
        }
    }
}
