<?php
/**
 * CsvParserTest class
 */
namespace LimesurveyExport\Test;

use LimesurveyExport\CsvParser;

/**
 * Test the CsvParser class
 */
class CsvParserTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Test the getGoogleSheetCsv function
     * @param  string $csv    CSV input
     * @param  string $result Expected result
     * @return void
     * @dataProvider csvProvider
     */
    public function testGetGoogleSheetCsv($csv, $result)
    {
        $parser = new CsvParser($csv);
        $this->assertEquals($result, $parser->getGoogleSheetCsv());
    }

    /**
     * Provide CSV examples for tests
     * @return array[]
     */
    public function csvProvider()
    {
        return [
            [
                'foo;bar',
                'foo,bar'.PHP_EOL
            ],
            [
                'foo;bar'.PHP_EOL.'bar; baz',
                'foo,bar'.PHP_EOL.'bar," baz"'.PHP_EOL
            ],
            [
                'foo[bar];foo[baz];foo[qux]'.PHP_EOL.'Oui;Non;Oui',
                'foo'.PHP_EOL.'"bar, qux"'.PHP_EOL
            ],
            [
                'foo[bar];foo[baz];foo[qux]'.PHP_EOL.'Foo;Bar;Baz',
                'foo'.PHP_EOL.'"bar : Foo'.PHP_EOL.'baz : Bar'.PHP_EOL.'qux : Baz"'.PHP_EOL
            ],
            [
                'foo;bar'.PHP_EOL.'foo',
                'foo,bar'.PHP_EOL.'foo,'.PHP_EOL
            ]
        ];
    }
}
