<?php
/**
 * ExportControllerTest class
 */
namespace LimesurveyExport\Test;

use LimesurveyExport\Controller\ExportController;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Test the ExportController class
 */
class ExportControllerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Initialize variables used in tests
     */
    protected function setUp()
    {
        $this->controller = new ExportController();
        $this->request = Request::createFromEnvironment(Environment::mock());
        $this->response = new Response();
    }

    /**
     * Test the exportCsv() function without a valid token
     * @return void
     */
    public function testExportCsvWithoutToken()
    {
        $result = $this->controller->exportCsv($this->request, $this->response, []);
        $this->assertTrue($result->isForbidden());
    }

    /**
     * Test the exportCsv() function
     * @return void
     */
    public function testExportCsv()
    {
        $this->markTestIncomplete('We need a way to test without a LimeSurvey install.');
    }
}
