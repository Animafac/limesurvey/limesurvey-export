<?php

use LimesurveyExport\Controller\ExportController;

require_once __DIR__.'/config.php';
require_once __DIR__.'/vendor/autoload.php';

$app = new \Slim\App();
$controller = new ExportController();
$app->get('/csv/{id}', [$controller, 'exportCsv']);
$app->run();
