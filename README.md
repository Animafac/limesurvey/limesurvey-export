# limesurvey-export

Simple HTTP API to export LimeSurvey survey responses

## Setup

First, you need to install dependencies with [Composer](https://getcomposer.org/):

```bash
composer install
```

And create the config file:

```bash
cp config.example.php config.php
```

Then edit `config.php` to add the LimeSurvey API credentials and a secret token that will be required to use this tool.

## Usage

You need to call `http://example.com/limesurvey-export/csv/[form id]?token=[token]` with a GET request.
It will return a CSV file with the surver responses.

## Grunt tasks

[Grunt](https://gruntjs.com/) can be used to run some automated tasks defined in `Gruntfile.js`.

Your first need to install JavaScript dependencies with [Yarn](https://yarnpkg.com/):

```bash
yarn install
```

### Lint

You can check that the JavaScript, JSON and PHP files are formatted correctly:

```bash
grunt lint
```

### Tests

You can run [PHPUnit](https://phpunit.de/) tests:

```bash
grunt test
```

### Documentation

[phpDocumentor](https://phpdoc.org/) can be used to generate the code documentation:

```bash
grunt doc
```

## CI

[Gitlab CI](https://docs.gitlab.com/ee/ci/) is used to run the tests automatically after each commit.
