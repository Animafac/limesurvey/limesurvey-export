<?php
/**
 * CsvParser class
 */
namespace LimesurveyExport;

use League\Csv\Reader;
use League\Csv\Writer;

/**
 * Manipulate CSV files imported from LimeSurvey
 */
class CsvParser
{

    /**
     * CSV file content
     * @var string
     */
    private $csv;

    /**
     * CsvParser constructor
     * @param string $csv CSV file content
     */
    public function __construct($csv)
    {
        $this->csv = $csv;
    }

    /**
     * Convert CSV headers to a more readable format
     *
     * LimeSurvey exports use a specific column title
     * format for multiple choice questions ("Question name"["Subquestion name"]).
     * As we regroup these columns we only want to keep the question name in the headers
     *
     * @param  string $header Column name
     * @return string Converted column name
     */
    private function convertHeader($header)
    {
        //Detect multiple choice question headers
        if (preg_match('/\[(.*)\]/', $header, $matches) && $matches[1] != 'Autre') {
            $header = trim(str_replace($matches[0], '', $header));
        }
        return $header;
    }

    /**
     * Parse the CSV file content into an associative array
     * @return array
     */
    private function parseLimeSurveyCsv()
    {
        $reader = Reader::createFromString($this->csv);
        $reader->setDelimiter(';');
        $export = [];
        //We extract and convert each CSV header
        foreach ($reader->fetch() as $row) {
            foreach ($row as $i => $header) {
                $header = $this->convertHeader($header);
                if (!isset($export[$header]) && !empty($header)) {
                    $export[$header] = [];
                }
            }
            break;
        }
        //We parse each CSV row and regroup values from multiple choice questions
        foreach ($reader->fetchAssoc(0) as $i => $row) {
            foreach ($row as $key => $value) {
                if (preg_match('/\[(.*)\]/', $key, $matches) && $matches[1] != 'Autre') {
                    $newKey = $this->convertHeader($key);
                    if ($value == 'Oui') {
                        if (isset($export[$newKey][$i])) {
                            $export[$newKey][$i] .= ', '.$matches[1];
                        } else {
                            $export[$newKey][$i] = $matches[1];
                        }
                    } elseif ($value !== 'Non') {
                        if (isset($export[$newKey][$i])) {
                            $export[$newKey][$i] .= PHP_EOL.$matches[1].' : '.$value;
                        } else {
                            $export[$newKey][$i] = $matches[1].' : '.$value;
                        }
                    }
                } elseif (!empty($key)) {
                    $export[$key][$i] = $value;
                }
            }
            foreach ($export as $key => $value) {
                if (!isset($export[$key][$i])) {
                    $export[$key][$i] = '';
                }
            }
        }
        return $export;
    }

    /**
     * Get a CSV output suitable for Google Sheets
     * @return string CSV
     */
    public function getGoogleSheetCsv()
    {
        $data = $this->parseLimeSurveyCsv();
        $writer = Writer::createFromString(new \SplTempFileObject());
        $writer->insertOne(array_keys($data));
        reset($data);
        $count = count($data[key($data)]) + 1;
        for ($i = 1; $i < $count; $i++) {
            $exportRow = [];
            foreach ($data as $row) {
                $exportRow[] = $row[$i];
            }
            $writer->insertOne($exportRow);
        }

        return (string) $writer;
    }
}
